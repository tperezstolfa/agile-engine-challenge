using System;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Shared;
using UniRx;

namespace ImagesGallery.Home.Infrastructure
{
	public class HttpHomeApi : HomeApi
	{
		readonly HttpApi httpApi;
		public const string GetImagesEndpoint = "/images";
		public const string PageQueryParamKey = "page";

		public HttpHomeApi(HttpApi httpApi)
		{
			this.httpApi = httpApi;
		}

		public IObservable<GalleryImagesPage> GetImages(int pageNumber)
		{
			var pageQueryParam = new Tuple<string, string>(PageQueryParamKey, pageNumber.ToString());

			return httpApi.Get(DefaultHttpApi.BaseEndpoint + GetImagesEndpoint, new[] {pageQueryParam})
				.Select(response => ImagesPageResponse.FromJson(response.Body))
				.Select(loginResponse => loginResponse.ToGalleryImagesPage());
		}
	}
}