﻿using Functional.Maybe;
using ImagesGallery.Home.Core.Services;

namespace ImagesGallery.Home.Infrastructure.Repositories
{
	public class InMemoryImagesPageRepository : ImagesPageRepository
	{
		int totalPages = 1;
		int currentPage;

		public Maybe<int> NextPage()
		{
			var nextPage = currentPage + 1;
			
			return nextPage > totalPages ? Maybe<int>.Nothing : nextPage.ToMaybe();
		}

		public void Update(int pageNumber, int pagesCount)
		{
			currentPage = pageNumber;
			totalPages = pagesCount;
		}

		public void Reset()
		{
			Update(0,1);
		}
	}
}