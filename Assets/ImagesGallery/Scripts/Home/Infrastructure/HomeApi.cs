using System;
using System.Collections.Generic;
using ImagesGallery.Home.Core.Domain;

namespace ImagesGallery.Home.Infrastructure
{
	public interface HomeApi
	{
		IObservable<GalleryImagesPage> GetImages(int pageNumber);
	}
}