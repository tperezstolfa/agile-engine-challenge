using System.Collections.Generic;
using System.Linq;
using ImagesGallery.Home.Core.Domain;
using Newtonsoft.Json;

public class ImagesPageResponse
{
	[JsonProperty("pictures")] public List<ImageResponse> Images;
	[JsonProperty("page")] public int PageNumber;
	[JsonProperty("pageCount")] public int PagesCount;
	[JsonProperty("hasMore")] public bool HasMorePages;

	public static ImagesPageResponse FromJson(string json)
	{
		return JsonConvert.DeserializeObject<ImagesPageResponse>(json);
	}

	public GalleryImagesPage ToGalleryImagesPage()
	{
		var images = Images.Select(imageResponse => imageResponse.ToGalleryImage()).ToList();
		return new GalleryImagesPage(images, PageNumber, PagesCount);
	}
}

public class ImageResponse
{
	[JsonProperty("id")] public string Id;
	[JsonProperty("cropped_picture")] public string Url;

	public GalleryImage ToGalleryImage()
	{
		return new GalleryImage(Id, Url);
	}
}