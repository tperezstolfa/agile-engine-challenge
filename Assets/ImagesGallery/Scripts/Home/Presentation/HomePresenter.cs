﻿using System;
using System.Collections.Generic;
using ImagesGallery.Home.Core.Actions;
using ImagesGallery.Home.Core.Domain;
using UniRx;

namespace ImagesGallery.Home.Presentation
{
	public class HomePresenter
	{
		readonly HomeScreenView view;
		readonly GetNextImages getNextImages;
		readonly ClearImagesCache clearImagesCache;

		public HomePresenter(HomeScreenView view, GetNextImages getNextImages, ClearImagesCache clearImagesCache)
		{
			this.view = view;
			this.getNextImages = getNextImages;
			this.clearImagesCache = clearImagesCache;

			this.view.OnRefresh().Subscribe(_ => ClearCacheAndShowNext());
			this.view.OnBottomReached().Subscribe(_ => ShowNextImages());
		}
		
		public void Present()
		{
			ShowNextImages();
		}

		void ClearCacheAndShowNext()
		{
			clearImagesCache.Do();
			ShowNextImages();
		}

		void ShowNextImages()
		{
			getNextImages.Do().Subscribe(view.Show);
		}
	}

	public interface HomeScreenView
	{
		void Show(List<GalleryImage> images);
		IObservable<Unit> OnRefresh();
		IObservable<Unit> OnBottomReached();
	}
}