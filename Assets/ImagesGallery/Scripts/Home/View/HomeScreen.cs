using System;
using System.Collections.Generic;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Home.Presentation;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace ImagesGallery.Home.View
{
	public class HomeScreen: MonoBehaviour, HomeScreenView
	{
		[SerializeField] ImageView imageViewPrefab;
		[SerializeField] ScrollRect imagesScroll;
		[SerializeField] Transform imagesContainer;
		[SerializeField] Button refreshButton;
		
		public void Show(List<GalleryImage> images)
		{
			gameObject.SetActive(true);
			images.ForEach(ShowImage);
		}

		public IObservable<Unit> OnRefresh()
		{
			return refreshButton.OnClickAsObservable().Do(_ => Clear());
		}
		
		public IObservable<Unit> OnBottomReached()
		{
			return imagesScroll.OnValueChangedAsObservable()
				.Where(normalizedPosition => normalizedPosition.y < 0.05f && imagesScroll.velocity.y > 10f)
				.Throttle(TimeSpan.FromSeconds(0.2))
				.AsUnitObservable();
		}

		void Clear()
		{
			foreach (Transform child in imagesContainer.transform) {
				Destroy(child.gameObject);
			}
		}

		void ShowImage(GalleryImage image)
		{
			var newImage = Instantiate(imageViewPrefab, imagesContainer);
			newImage.Show(image);
		}
	}
}