using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Shared;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace ImagesGallery.Home.View
{
	internal class ImageView : MonoBehaviour
	{
		[SerializeField] RawImage image;
		public void Show(GalleryImage galleryImage)
		{
			image.gameObject.SetActive(false);
			UnityTexture2dDownloader.Load(galleryImage.Url).Subscribe(texture2D =>
			{
				image.gameObject.SetActive(true);
				image.texture = texture2D;
			});
		}
	}
}