﻿using System;
using System.Collections.Generic;
using Functional.Maybe;
using ImagesGallery.Home.Core.Actions;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Home.Infrastructure;
using UniRx;

namespace ImagesGallery.Home.Core.Services
{
	public class RemoteImagesService : ImagesService
	{
		readonly HomeApi homeApi;
		readonly ImagesPageRepository imagesPageRepository;

		public RemoteImagesService(HomeApi homeApi, ImagesPageRepository imagesPageRepository)
		{
			this.homeApi = homeApi;
			this.imagesPageRepository = imagesPageRepository;
		}

		public IObservable<List<GalleryImage>> GetNextImages()
		{
			return imagesPageRepository.NextPage()
				.Select(GetImagesForPage)
				.OrElse(Observable.Return(new List<GalleryImage>()));
		}

		public void ClearPagesCache()
		{
			imagesPageRepository.Reset();
		}

		IObservable<List<GalleryImage>> GetImagesForPage(int pageNumber)
		{
			return homeApi.GetImages(pageNumber)
				.Do(page => imagesPageRepository.Update(page.PageNumber, page.PagesCount))
				.Select(page => page.GalleryImages);
		}
	}
}