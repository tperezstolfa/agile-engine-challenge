using Functional.Maybe;

namespace ImagesGallery.Home.Core.Services
{
	public interface ImagesPageRepository
	{
		Maybe<int> NextPage();
		void Update(int pageNumber, int pagesCount);
		void Reset();
	}
}