using System;
using System.Collections.Generic;
using ImagesGallery.Home.Core.Domain;

namespace ImagesGallery.Home.Core.Services
{
	public interface ImagesService
	{
		IObservable<List<GalleryImage>> GetNextImages();
		void ClearPagesCache();
	}
}