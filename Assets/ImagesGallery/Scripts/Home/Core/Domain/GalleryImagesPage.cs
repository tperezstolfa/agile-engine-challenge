using System.Collections.Generic;

namespace ImagesGallery.Home.Core.Domain
{
	public class GalleryImagesPage
	{
		public readonly List<GalleryImage> GalleryImages;
		public readonly int PageNumber;
		public readonly int PagesCount;

		public GalleryImagesPage(List<GalleryImage> galleryImages, int pageNumber, int pagesCount)
		{
			GalleryImages = galleryImages;
			PageNumber = pageNumber;
			PagesCount = pagesCount;
		}

		protected bool Equals(GalleryImagesPage other)
		{
			var listsEqual = GalleryImages.TrueForAll(other.GalleryImages.Contains) &&
			                 GalleryImages.Count == other.GalleryImages.Count;
			return listsEqual && PageNumber == other.PageNumber && PagesCount == other.PagesCount;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((GalleryImagesPage) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (GalleryImages != null ? GalleryImages.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ PageNumber;
				hashCode = (hashCode * 397) ^ PagesCount;
				return hashCode;
			}
		}
	}
}