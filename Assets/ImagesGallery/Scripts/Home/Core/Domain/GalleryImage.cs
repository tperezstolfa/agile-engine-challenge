﻿namespace ImagesGallery.Home.Core.Domain
{
	public class GalleryImage
	{
		public readonly string Id;
		public readonly string Url;

		public GalleryImage(string id, string url)
		{
			Id = id;
			Url = url;
		}

		protected bool Equals(GalleryImage other)
		{
			return string.Equals(Id, other.Id) && string.Equals(Url, other.Url);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((GalleryImage) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Id != null ? Id.GetHashCode() : 0) * 397) ^ (Url != null ? Url.GetHashCode() : 0);
			}
		}
	}
}
