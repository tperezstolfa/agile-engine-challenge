using System;
using System.Collections.Generic;
using Functional.Maybe;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Home.Core.Services;
using JetBrains.Annotations;

namespace ImagesGallery.Home.Core.Actions
{
	public class GetNextImages
	{
		readonly ImagesService imagesService;

		[UsedImplicitly]
		protected GetNextImages()
		{
			//Test framework requires a parameterless constructor
		}

		public GetNextImages(ImagesService imagesService)
		{
			this.imagesService = imagesService;
		}

		public virtual IObservable<List<GalleryImage>> Do()
		{
			return imagesService.GetNextImages();
		}
	}
}