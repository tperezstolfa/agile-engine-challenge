using System;
using ImagesGallery.Home.Core.Services;
using JetBrains.Annotations;

namespace ImagesGallery.Home.Core.Actions
{
	public class ClearImagesCache
	{
		readonly ImagesService imagesService;

		[UsedImplicitly]
		protected ClearImagesCache()
		{
			//Test framework requires a parameterless constructor
		}

		public ClearImagesCache(ImagesService imagesService)
		{
			this.imagesService = imagesService;
		}

		public virtual void Do()
		{
			imagesService.ClearPagesCache();
		}
	}
}