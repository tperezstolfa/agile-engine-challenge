using System;
using TheLair.HTTP;

namespace ImagesGallery.Shared
{
	public interface HttpApi
	{
		IObservable<HttpResponse> Post<T>(string url, T request);
		IObservable<HttpResponse>  Get(string getImagesEndpoint, Tuple<string,string>[] queryParams);
	}
}