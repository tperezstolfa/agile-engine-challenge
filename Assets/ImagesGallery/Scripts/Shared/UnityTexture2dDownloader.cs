using System;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

namespace ImagesGallery.Shared
{
	public static class UnityTexture2dDownloader
	{
		public static IObservable<Texture2D> Load(string url)
		{
			var loadingImageWebRequest = DownloadTextureRequest(url);

			return DownloadTexture(url, loadingImageWebRequest);;
		}

		static IObservable<Texture2D> DownloadTexture(string url, UnityWebRequest loadingImageWebRequest)
		{
			return Observable.EveryUpdate()
				.First(_ => loadingImageWebRequest.isDone)
				.AsUnitObservable()
				.Select(_ => {
					if (loadingImageWebRequest.responseCode != 200)
						throw new ArgumentException("No image in " + url);

					return DownloadHandlerTexture.GetContent(loadingImageWebRequest);
				});
		}

		static UnityWebRequest DownloadTextureRequest(string url)
		{
			UnityWebRequest loadingImageWebRequest;
			try {
				loadingImageWebRequest = UnityWebRequestTexture.GetTexture(url);
				loadingImageWebRequest.SendWebRequest();
			}
			catch (Exception e) {
				loadingImageWebRequest = null;
				Debug.LogErrorFormat("Error trying to start image download from {0} : {1}", url, e.Message);
			}

			return loadingImageWebRequest;
		}
	}
}