using System;
using Newtonsoft.Json;
using TheLair.HTTP;

namespace ImagesGallery.Shared
{
	public class DefaultHttpApi : HttpApi
	{
		const int RequestTimeout = 2;
		public const string BaseEndpoint = "http://195.39.233.28:8035";


		public IObservable<HttpResponse> Post<T>(string url, T body)
		{
			var request = HttpBuilder.Post(url, JsonConvert.SerializeObject(body));

			return Send(request);
		}

		public IObservable<HttpResponse> Get(string url, Tuple<string, string>[] queryParams)
		{
			var request = HttpBuilder.Get(url);

			foreach (var parameter in queryParams)
				request.AddParam(parameter.Item1, parameter.Item2);

			return Send(request);
		}

		protected virtual IObservable<HttpResponse> Send(HttpBuilder request)
		{
			return request.Send(RequestTimeout);
		}
	}
}