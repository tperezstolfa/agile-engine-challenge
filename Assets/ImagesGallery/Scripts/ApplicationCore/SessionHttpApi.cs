using System;
using ImagesGallery.Authentication.Core.Service;
using ImagesGallery.Shared;
using TheLair.HTTP;
using UniRx;

namespace ImagesGallery
{
	public class SessionHttpApi : DefaultHttpApi
	{
		readonly SessionTokenRepository tokenRepository;
		LoginApi loginApi;

		public SessionHttpApi(SessionTokenRepository tokenRepository)
		{
			this.tokenRepository = tokenRepository;
		}

		protected override IObservable<HttpResponse> Send(HttpBuilder request)
		{
			AddAuthenticationHeader(request);

			return base.Send(request)
				.CatchHttpError(403, _ => HandleUnauthorizedError(request));
		}

		void AddAuthenticationHeader(HttpBuilder request)
		{
			var currentToken = tokenRepository.Get();
			request.AddHeader("Authorization", "Bearer " + currentToken.Token);
		}

		IObservable<HttpResponse> HandleUnauthorizedError(HttpBuilder request)
		{
			return loginApi.Authenticate()
				.SelectMany(_ => Send(request));
		}
	}
}