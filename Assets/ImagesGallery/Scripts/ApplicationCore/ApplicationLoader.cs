﻿using ImagesGallery.Authentication.Core.Actions;
using ImagesGallery.Authentication.Core.Service;
using ImagesGallery.Authentication.Infrastructure;
using ImagesGallery.Home.Core.Actions;
using ImagesGallery.Home.Core.Services;
using ImagesGallery.Home.Infrastructure;
using ImagesGallery.Home.Infrastructure.Repositories;
using ImagesGallery.Home.Presentation;
using ImagesGallery.Home.View;
using ImagesGallery.Shared;
using UniRx;
using UnityEngine;

namespace ImagesGallery.ApplicationCore
{
	public class ApplicationLoader : MonoBehaviour
	{
		[SerializeField] HomeScreen homeScreenView;

		void Start()
		{
			//Instantiation and dependency injection of Authentication module
			var sessionTokenRepository = new InMemorySessionTokenRepository();
			LoginApi loginApi = new HttpLoginApi(new DefaultHttpApi());
			var authenticate = new Authenticate(new SessionLoginService(loginApi, sessionTokenRepository));

			//Instantiation and dependency injection of Home module
			HttpApi sessionHttpApi = new SessionHttpApi(sessionTokenRepository);
			HomeApi homeApi = new HttpHomeApi(sessionHttpApi);
			ImagesService imagesService = new RemoteImagesService(homeApi, new InMemoryImagesPageRepository());
			var getImagesPage = new GetNextImages(imagesService);
			var clearImagesCache = new ClearImagesCache(imagesService);
			var homePresenter = new HomePresenter(homeScreenView, getImagesPage, clearImagesCache);

			//Application start
			authenticate.Do().Subscribe(_ => homePresenter.Present());
		}
	}
}