using Newtonsoft.Json;

namespace ImagesGallery.Authentication.Infrastructure.Request
{
	public class LoginRequest
	{
		[JsonProperty("apiKey")] public string ApiKey;

		public LoginRequest(string apiKey)
		{
			ApiKey = apiKey;
		}
	}
}