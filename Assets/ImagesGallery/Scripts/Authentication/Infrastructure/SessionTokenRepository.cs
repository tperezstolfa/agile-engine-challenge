using ImagesGallery.Authentication.Core.Domain;

namespace ImagesGallery.Authentication.Core.Service
{
	public interface SessionTokenRepository
	{
		void Put(AuthenticationToken sessionToken);
		AuthenticationToken Get();
	}
}