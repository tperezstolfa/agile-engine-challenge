using ImagesGallery.Authentication.Core.Domain;
using Newtonsoft.Json;

namespace ImagesGallery.Authentication.Infrastructure.Response
{
	public class LoginResponse
	{
		[JsonProperty("auth")] public bool AuthenticationSuccessful;

		[JsonProperty("token")] public string Token;

		public static LoginResponse FromJson(string json)
		{
			return JsonConvert.DeserializeObject<LoginResponse>(json);
		}

		public AuthenticationToken ToAuthenticationToken()
		{
			return new AuthenticationToken(Token);
		}
	}
}