using ImagesGallery.Authentication.Core.Domain;
using ImagesGallery.Authentication.Core.Service;

namespace ImagesGallery.Authentication.Infrastructure
{
	public class InMemorySessionTokenRepository : SessionTokenRepository
	{
		AuthenticationToken sessionToken;

		public void Put(AuthenticationToken sessionToken)
		{
			this.sessionToken = sessionToken;
		}

		public AuthenticationToken Get()
		{
			return sessionToken;
		}
	}
}