using System;
using ImagesGallery.Authentication.Core.Domain;
using ImagesGallery.Authentication.Core.Service;
using ImagesGallery.Authentication.Infrastructure.Request;
using ImagesGallery.Authentication.Infrastructure.Response;
using ImagesGallery.Shared;
using UniRx;

namespace ImagesGallery.Authentication.Infrastructure
{
	public class HttpLoginApi : LoginApi
	{
		const string ApiKey = "23567b218376f79d9415";
		
		readonly HttpApi api;

		public HttpLoginApi(HttpApi api)
		{
			this.api = api;
		}

		public const string LoginEndpoint = "/auth";

		public IObservable<AuthenticationToken> Authenticate()
		{
			const string url = DefaultHttpApi.BaseEndpoint + LoginEndpoint;
			var request = new LoginRequest(ApiKey);
			return api.Post(url, request)
				.Select(response => LoginResponse.FromJson(response.Body))
				.Select(loginResponse => loginResponse.ToAuthenticationToken());
		}
	}
}