using System;
using ImagesGallery.Authentication.Core.Domain;
using UniRx;

namespace ImagesGallery.Authentication.Core.Service
{
	public class SessionLoginService : LoginService
	{
		readonly LoginApi api;
		readonly SessionTokenRepository sessionTokenRepository;

		public SessionLoginService(LoginApi api, SessionTokenRepository sessionTokenRepository)
		{
			this.api = api;
			this.sessionTokenRepository = sessionTokenRepository;
		}

		public IObservable<Unit> Authenticate()
		{
			return api.Authenticate()
				.Do(StoreValidToken)
				.AsUnitObservable();
		}

		void StoreValidToken(AuthenticationToken token)
		{
			sessionTokenRepository.Put(token);
		}
	}
}