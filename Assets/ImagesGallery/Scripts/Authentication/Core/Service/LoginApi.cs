using System;
using ImagesGallery.Authentication.Core.Domain;

namespace ImagesGallery.Authentication.Core.Service
{
	public interface LoginApi
	{
		IObservable<AuthenticationToken> Authenticate();
	}
}