using System;
using UniRx;
using UnityEngine;

namespace ImagesGallery.Authentication.Core.Service
{
	public class DummyAuthenticationService : LoginService
	{
		public IObservable<Unit> Authenticate()
		{
			Debug.Log("Application should be authenticating now");
            
			return Observable.ReturnUnit();
		}
	}
}