using System;
using UniRx;

namespace ImagesGallery.Authentication.Core.Service
{
	public interface LoginService
	{
		IObservable<Unit> Authenticate();
	}
}