namespace ImagesGallery.Authentication.Core.Domain
{
	public class AuthenticationToken
	{
		public readonly string Token;

		public AuthenticationToken(string token)
		{
			Token = token;
		}

	}
}