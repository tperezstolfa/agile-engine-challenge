using System;
using ImagesGallery.Authentication.Core.Service;
using UniRx;

namespace ImagesGallery.Authentication.Core.Actions
{
	public class Authenticate
	{
		readonly LoginService loginService;

		public Authenticate(LoginService loginService)
		{
			this.loginService = loginService;
		}

		public IObservable<Unit> Do()
		{
			return loginService.Authenticate();
		}
	}
}