using System;
using System.Collections.Generic;
using Functional.Maybe;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Home.Infrastructure;
using NSubstitute;
using NUnit.Framework;
using UniRx;

namespace ImagesGallery.Home.Core.Services
{
	public class RemoteImageServiceTest
	{
		HomeApi homeApi;
		RemoteImagesService remoteImagesService;
		readonly GalleryImage firstImage = new GalleryImage("id", "url");
		readonly GalleryImage secondImage = new GalleryImage("id", "url");
		ImagesPageRepository imagesPageRepository;

		[SetUp]
		public void BeforeEach()
		{
			homeApi = Substitute.For<HomeApi>();
			imagesPageRepository = Substitute.For<ImagesPageRepository>();

			remoteImagesService = new RemoteImagesService(homeApi, imagesPageRepository);
		}

		[Test]
		public void retrieve_images_for_next_page()
		{
			var nextPage = GivenThereIsNextPage();
			var expectedImages = GivenImagesWillBeRetrievedForPage(nextPage);

			WhenImagesAreRetrieved().Subscribe(retrievedImages =>
			{
				ThenRetrievedImagesAreTheExpected(expectedImages.GalleryImages, retrievedImages);
			});

			ThenUpdatesNextPageRepository(expectedImages.PageNumber, expectedImages.PagesCount);
		}

		[Test]
		public void wont_retrieve_images_if_no_more_pages()
		{
			GivenThereIsNotNextPage();

			WhenImagesAreRetrieved().Subscribe(retrievedImages =>
			{
				ThenImagesAreNotRetrieved();
				ThenReturnsNothing(retrievedImages);
			});
		}

		[Test]
		public void clear_pages_cache()
		{
			WhenClearsPagesCache();

			ThenPagesCacheIsReset();
		}

		void GivenThereIsNotNextPage()
		{
			imagesPageRepository.NextPage().Returns(Maybe<int>.Nothing);
		}

		int GivenThereIsNextPage()
		{
			const int nextPage = 1;
			imagesPageRepository.NextPage().Returns(nextPage.ToMaybe());

			return nextPage;
		}

		GalleryImagesPage GivenImagesWillBeRetrievedForPage(int pageNumber)
		{
			var images = new List<GalleryImage> {firstImage, secondImage};
			var galleryImagesPage = new GalleryImagesPage(images, 1, 10);
			homeApi.GetImages(pageNumber).Returns(Observable.Return(galleryImagesPage));
			return galleryImagesPage;
		}

		IObservable<List<GalleryImage>> WhenImagesAreRetrieved()
		{
			return remoteImagesService.GetNextImages();
		}

		void WhenClearsPagesCache()
		{
			remoteImagesService.ClearPagesCache();
		}

		static void ThenRetrievedImagesAreTheExpected(List<GalleryImage> expectedImages,
			List<GalleryImage> retrievedImages)
		{
			Assert.IsTrue(expectedImages.TrueForAll(retrievedImages.Contains));
		}

		void ThenUpdatesNextPageRepository(int pageNumber, int pagesCount)
		{
			imagesPageRepository.Received(1).Update(pageNumber, pagesCount);
		}

		void ThenReturnsNothing(List<GalleryImage> retrievedImages)
		{
			Assert.IsEmpty(retrievedImages);
		}

		void ThenImagesAreNotRetrieved()
		{
			homeApi.DidNotReceive().GetImages(Arg.Any<int>());
		}

		void ThenPagesCacheIsReset()
		{
			imagesPageRepository.Reset();
		}
	}
}