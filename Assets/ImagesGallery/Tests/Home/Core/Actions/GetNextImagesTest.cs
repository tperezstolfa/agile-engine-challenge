using System;
using System.Collections.Generic;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Home.Core.Services;
using NSubstitute;
using NUnit.Framework;
using UniRx;

namespace ImagesGallery.Home.Core.Actions
{
	public class GetNextImagesTest
	{
		GetNextImages getNextImages;
		ImagesService imagesService;
		readonly GalleryImage firstImage = new GalleryImage("id", "url");
		readonly GalleryImage secondImage = new GalleryImage("id", "url");

		[SetUp]
		public void BeforeEach()
		{
			imagesService = Substitute.For<ImagesService>();

			getNextImages = new GetNextImages(imagesService);
		}

		[Test]
		public void retrieve_images_for_valid_page()
		{
			var expectedImages = GivenImagesWillBeRetrieved();

			WhenPageOfImagesIsRetrieved(1).Subscribe(retrievedImages =>
			{
				ThenRetrievedImagesAreTheExpected(expectedImages, retrievedImages);
			});
		}

		List<GalleryImage> GivenImagesWillBeRetrieved()
		{
			var images = new List<GalleryImage> {firstImage, secondImage};
			imagesService.GetNextImages();
			return images;
		}

		IObservable<List<GalleryImage>> WhenPageOfImagesIsRetrieved(int pageNumber)
		{
			return getNextImages.Do();
		}

		static void ThenRetrievedImagesAreTheExpected(List<GalleryImage> expectedImages, List<GalleryImage> retrievedImages)
		{
			Assert.IsTrue(expectedImages.TrueForAll(retrievedImages.Contains));
		}
	}
}