using ImagesGallery.Home.Core.Services;
using NSubstitute;
using NUnit.Framework;

namespace ImagesGallery.Home.Core.Actions
{
	public class ClearImagesCacheTest
	{
		ImagesService imagesService;
		ClearImagesCache clearImagesCache;

		[SetUp]
		public void BeforeEach()
		{
			imagesService = Substitute.For<ImagesService>();
			clearImagesCache = new ClearImagesCache(imagesService);
		}

		[Test]
		public void clears_cache()
		{
			clearImagesCache.Do();

			imagesService.Received(1).ClearPagesCache();
		}
	}
}