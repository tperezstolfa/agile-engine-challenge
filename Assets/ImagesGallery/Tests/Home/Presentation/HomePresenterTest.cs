﻿using System.Collections.Generic;
using ImagesGallery.Home.Core.Actions;
using ImagesGallery.Home.Core.Domain;
using NSubstitute;
using NUnit.Framework;
using UniRx;

namespace ImagesGallery.Home.Presentation
{
	public class HomePresenterTest
	{
		HomePresenter presenter;
		HomeScreenView view;
		GetNextImages getNextImages;
		static readonly List<GalleryImage> someImages = new List<GalleryImage>() {new GalleryImage("id", "url")};
		static readonly List<GalleryImage> moreImages = new List<GalleryImage>() {new GalleryImage("id2", "url2")};
		ClearImagesCache clearImagesCache;


		[SetUp]
		public void BeforeEach()
		{
			view = Substitute.For<HomeScreenView>();
			getNextImages = Substitute.For<GetNextImages>();
			clearImagesCache = Substitute.For<ClearImagesCache>();
		}

		[Test]
		public void present_first_page()
		{
			GivenNextImages(someImages);
			GivenAPresenter();

			WhenPresenting();

			ThenImagesWereRequested();
			ThenViewDisplaysImages(someImages);
		}

		[Test]
		public void present_again_on_refresh()
		{
			var refreshNotifier = GivenViewWillRefresh();
			GivenNextImages(someImages);
			GivenAPresenter();

			WhenViewNotifiesRefresh(refreshNotifier);

			ThenImagesCacheIsCleared();
			ThenImagesWereRequested();
			ThenViewDisplaysImages(someImages);
		}

		[Test]
		public void load_next_images_on_view_reached_last_page()
		{
			var lastPageNotifier = GivenViewWillReachBottom();
			GivenNextImages(moreImages);
			GivenAPresenter();

			WhenViewNotifiesLastImageReached(lastPageNotifier);

			ThenViewDisplaysImages(moreImages);
		}

		Subject<Unit> GivenViewWillRefresh()
		{
			var subject = new Subject<Unit>();
			view.OnRefresh().Returns(subject);

			return subject;
		}

		Subject<Unit> GivenViewWillReachBottom()
		{
			var subject = new Subject<Unit>();
			view.OnBottomReached().Returns(subject);

			return subject;
		}

		void GivenAPresenter()
		{
			presenter = new HomePresenter(view, getNextImages, clearImagesCache);
		}

		void GivenNextImages(List<GalleryImage> images)
		{
			getNextImages.Do().Returns(Observable.Return(images));
		}

		void WhenPresenting()
		{
			presenter.Present();
		}

		static void WhenViewNotifiesRefresh(Subject<Unit> notifier)
		{
			notifier.OnNext(Unit.Default);
		}

		static void WhenViewNotifiesLastImageReached(Subject<Unit> notifier)
		{
			notifier.OnNext(Unit.Default);
		}

		void ThenViewDisplaysImages(List<GalleryImage> images)
		{
			view.Received(1).Show(images);
		}

		void ThenImagesWereRequested()
		{
			getNextImages.Received(1).Do();
		}

		void ThenImagesCacheIsCleared()
		{
			clearImagesCache.Received(1).Do();
		}
	}
}