using Functional.Maybe;
using ImagesGallery.Home.Infrastructure.Repositories;
using NUnit.Framework;

namespace ImagesGallery.Home.Infrastructure
{
	public class ImMemoryImagesPageRepositoryTest
	{
		InMemoryImagesPageRepository repository;

		[SetUp]
		public void BeforeEach()
		{
			repository = new InMemoryImagesPageRepository();
		}

		[Test]
		public void returns_first_page_the_first_time()
		{
			var nextPage = repository.NextPage();
			
			Assert.AreEqual(1, nextPage.Value);
		}
		
		[Test]
		public void returns_next_page_if_already_updated()
		{
			repository.Update(1, 10);
			
			var nextPage = repository.NextPage();
			
			Assert.AreEqual(2, nextPage.Value);
		}
		
		[Test]
		public void returns_nothing_if_in_last_page()
		{
			repository.Update(10, 10);
			
			var nextPage = repository.NextPage();
			
			Assert.IsTrue(nextPage.IsNothing());
		}

		[Test]
		public void after_reset_returns_the_first_page()
		{
			repository.NextPage();
			repository.Reset();
			
			var nextPage = repository.NextPage();
			
			Assert.AreEqual(1, nextPage.Value);
		}
	}
}