using System;
using System.Collections.Generic;
using System.Linq;
using ImagesGallery.Home.Core.Domain;
using ImagesGallery.Shared;
using NSubstitute;
using NUnit.Framework;
using TheLair.HTTP;
using UniRx;

namespace ImagesGallery.Home.Infrastructure
{
	public class HttpHomeApiTest
	{
		HttpApi api;
		HttpHomeApi homeApi;


		[SetUp]
		public void BeforeEach()
		{
			api = Substitute.For<HttpApi>();

			homeApi = new HttpHomeApi(api);
		}

		[Test]
		public void get_images_page()
		{
			const string picture1Id = "picture1";
			const string picture1Url = "picture1Url";
			const string picture2Id = "picture2";
			const string picture2Url = "picture2Url";
			const int pageNumber = 3;
			const int pagesCount = 23;

			var rawResponseBody =
				"{\"pictures\":[" +
				"{\"id\":\"" + picture1Id + "\",\"cropped_picture\":\"" + picture1Url + "\"}," +
				"{\"id\":\"" + picture2Id + "\",\"cropped_picture\":\"" + picture2Url + "\"}]," +
				"\"page\":" + pageNumber + "," +
				"\"pageCount\":" + pagesCount + "," +
				"\"hasMore\":true}";
			var expectedImagesPage = new GalleryImagesPage(
				new List<GalleryImage>
					{new GalleryImage(picture1Id, picture1Url), new GalleryImage(picture2Id, picture2Url)},
				pageNumber,
				pagesCount);

			GivenImagesResponse(pageNumber, rawResponseBody);

			homeApi.GetImages(pageNumber).Subscribe(receivedPage =>
			{
				ThenPageIsTheExpected(expectedImagesPage, receivedPage);
			});
		}

		void ThenPageIsTheExpected(GalleryImagesPage expectedImagesPage, GalleryImagesPage receivedPage)
		{
			Assert.IsTrue(expectedImagesPage.GalleryImages
				.TrueForAll(image => receivedPage.GalleryImages.Contains(image)));
			Assert.AreEqual(expectedImagesPage.PageNumber, receivedPage.PageNumber);
			Assert.AreEqual(expectedImagesPage.PagesCount, receivedPage.PagesCount);
		}

		void GivenImagesResponse(int pageNumber, string responseBody)
		{
			var httpResponse = new HttpResponse(200, responseBody, null, null);
			api.Get(DefaultHttpApi.BaseEndpoint + HttpHomeApi.GetImagesEndpoint,
					Arg.Is<Tuple<string, string>[]>(queryParams =>
						QueryParamsContainTheRightPageParam(queryParams, pageNumber)))
				.Returns(Observable.Return(httpResponse));
		}

		static bool QueryParamsContainTheRightPageParam(Tuple<string, string>[] queryParams, int pageNumber)
		{
			return queryParams.Any(queryParam =>
				queryParam.Item1 == HttpHomeApi.PageQueryParamKey
				&& queryParam.Item2 == pageNumber.ToString());
		}
	}
}