using ImagesGallery.Authentication.Core.Domain;
using ImagesGallery.Authentication.Infrastructure.Request;
using ImagesGallery.Shared;
using NSubstitute;
using NUnit.Framework;
using TheLair.HTTP;
using UniRx;

namespace ImagesGallery.Authentication.Infrastructure
{
	public class HttpLoginApiTest
	{
		HttpApi api;
		HttpLoginApi loginApi;

		[SetUp]
		public void BeforeEach()
		{
			api = Substitute.For<HttpApi>();
			loginApi = new HttpLoginApi(api);
		}

		[Test]
		public void authenticate_successfully()
		{
			var validToken = "validToken";
			var rawResponseBody = "{\"auth\":true,\"token\":\"" + validToken + "\"}";

			GivenAuthenticationResponse(rawResponseBody);

			loginApi.Authenticate().Subscribe(receivedToken =>
			{
				ThenTokenIsTheExpected(validToken, receivedToken);
			});
		}

		static void ThenTokenIsTheExpected(string validToken, AuthenticationToken receivedToken)
		{
			Assert.AreEqual(validToken, receivedToken.Token);
		}

		void GivenAuthenticationResponse(string responseBody)
		{
			var httpResponse = new HttpResponse(200, responseBody, null, null);
			api.Post(DefaultHttpApi.BaseEndpoint + HttpLoginApi.LoginEndpoint, Arg.Any<LoginRequest>())
				.Returns(Observable.Return(httpResponse));
		}
	}
}