﻿using System;
using ImagesGallery.Authentication.Core.Service;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using UnityEngine;

namespace ImagesGallery.Authentication.Core.Actions
{
	public class AuthenticateTest : MonoBehaviour
	{
		Authenticate authenticate;
		LoginService loginService;

		[SetUp]
		public void BeforeEach()
		{
			loginService = Substitute.For<LoginService>();
		
			authenticate = new Authenticate(loginService);
		}
	
		[Test]
		public void authenticates_ok()
		{
			var authenticationResult = false;
			GivenASuccessfulAuthentication();
		
			WhenAuthenticates().Subscribe(_ => authenticationResult=true);

			ThenDidAttemptAuthentication();
			ThenAuthenticatesSuccessfully(authenticationResult);
		}

		static void ThenAuthenticatesSuccessfully(bool authenticationResult)
		{
			Assert.IsTrue(authenticationResult);
		}

		void GivenASuccessfulAuthentication()
		{
			loginService.Authenticate().Returns(Observable.ReturnUnit());
		}

		IObservable<Unit> WhenAuthenticates()
		{
			return authenticate.Do();
		}

		void ThenDidAttemptAuthentication()
		{
			loginService.Received(1).Authenticate();
		}
	}
}
