using System;
using ImagesGallery.Authentication.Core.Domain;
using ImagesGallery.Authentication.Core.Service;
using NSubstitute;
using NUnit.Framework;
using UniRx;

namespace ImagesGallery.Authentication.Core.Services
{
	public class SessionLoginServiceTest
	{
		SessionLoginService sessionLoginService;
		LoginApi api;
		SessionTokenRepository sessionTokenRepository;

		[SetUp]
		public void BeforeEach()
		{
			api = Substitute.For<LoginApi>();
			sessionTokenRepository = Substitute.For<SessionTokenRepository>();
			sessionLoginService = new SessionLoginService(api, sessionTokenRepository);
		}

		[Test]
		public void login_successful()
		{
			var expectedAuthenticationToken = new AuthenticationToken("validToken");
			GivenASuccessfulAuthentication(expectedAuthenticationToken);

			WhenAuthenticates();

			ThenTokenIsStored(expectedAuthenticationToken);
		}

		void ThenTokenIsStored(AuthenticationToken actualReceivedToken)
		{
			sessionTokenRepository.Received(1).Put(actualReceivedToken);
		}

		void GivenASuccessfulAuthentication(AuthenticationToken sessionToken)
		{
			api.Authenticate().Returns(Observable.Return(sessionToken));
		}

		void WhenAuthenticates()
		{
			sessionLoginService.Authenticate().Subscribe();
		}
	}
}