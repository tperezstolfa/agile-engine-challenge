# Images gallery code challenge

This code challenge resolves the requirements of the code challenge but instead of using react native, I used unity.

You can take it more like a code-sample than the challenge itself, because the times estimated for the challenge in react-native are unachiavable . As a matter of fact, I didn't even finish all the requirements, but I think there is more than enough code as it is to evaluate my skills.

All the scripts and tests on this project can be found inside the folder Assets/ImagesGallery. You may also find some external packages, the ones that are authored by Tizi and under the com.thelair scope, are some of my packages hosted on my own npm server that I integrate with Unity Package Manager so I can access my code in any project.

To run the project, you will need Unity 2018.4.16f1 or later. 
You need to: 
Clone the repo, 
open with Unity, 
load the Main scene found in Assets/Scenes/Main.unity, 
click on play, 
and you are good to go!


The project can be tested on the Unity editor by hitting the play button, but if you want you can deploy it to a device following the oficial Unity instructions https://learn.unity.com/tutorial/building-for-mobile#5c7f8528edbc2a002053b4a1


